const got = require("./data.js")

function countAllPeople() {
    try {
        if(typeof got !== "object") {
            throw new Error("Please provide the correct format of the data!!!");
        }
        const total_number_of_people = Object.keys(got).map((each_house) => {
            return Object.keys(got[each_house]).reduce((previous , current) => {
                return previous += got[each_house][current].people.length;
            } , 0);
        });
        return total_number_of_people[0];
    } catch(error) {
        console.error("Error occured : " , error.message);
    }
}

module.exports = countAllPeople;