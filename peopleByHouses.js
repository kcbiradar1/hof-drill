const got = require("./data.js")

function peopleByHouses() {
    try {
        if(typeof got !== "object") {
            throw new Error("Please provide the correct format of the data!!!");
        }
        const number_of_people_by_houses = Object.keys(got).map((each_house) => {
            return Object.keys(got[each_house]).reduce((previous , current) => {
                if(!previous[got[each_house][current].name]) {
                    previous[got[each_house][current].name] = got[each_house][current].people.length; 
                }
                return previous;
            } , {});
        })
        return number_of_people_by_houses;
    } catch(error) {
        console.error("Error occured : " , error.message);
    }
}

module.exports = peopleByHouses;