const got = require("./data.js");

function nameWithS() {
  try {
    if (typeof got !== "object") {
      throw new Error("Please provide the correct format of the data!!!");
    }
    const names_of_all_the_people = Object.keys(got).map((each_house) => {
      return Object.keys(got[each_house]).reduce((previous, current) => {
        people_array = got[each_house][current].people.reduce(
          (previous_two, current_two) => {
            previous_two.push(current_two.name);
            return previous_two;
          },
          []
        );
        previous.push(people_array);
        return previous;
      }, []);
    });
    const result = names_of_all_the_people.flat(Infinity);
    const nameWithInlcudeS = result.reduce((previous , current) => {
        if(current.includes('S') || current.includes('s')) {
            previous.push(current);
        }
        return previous;
    } , []);
    return nameWithInlcudeS;
  } catch (error) {
    console.error("Error occured : ", error.message);
  }
}

module.exports = nameWithS;
